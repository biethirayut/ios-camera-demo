//
//  ViewController.swift
//  HAHA
//
//  Created by Thirayut on 30/4/2563 BE.
//  Copyright © 2563 test. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVFoundation
import Photos

class ViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var cameraButton: UIButton!

  var imagePicker: UIImagePickerController!

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      imagePicker.dismiss(animated: true, completion: nil)
      imageView.image = info[.originalImage] as? UIImage
  }

  @IBAction func cameraTapped(_ sender: Any) {
    imagePicker =  UIImagePickerController()
    imagePicker.delegate = self
    imagePicker.sourceType = .camera
    imagePicker.allowsEditing = true

    present(imagePicker, animated: true, completion: nil)
  }

}
